---
layout: handbook-page-toc
title: Social Advocacy Curator Program
description: Strategies and details to enable curators to share GitLab-related news for company-wide enablement
twitter_image: /images/opengraph/handbook/social-marketing/social-advocacy-curator-program-opengraph.png
twitter_image_alt: GitLab's Social Media Handbook branded image
twitter_site: gitlab
twitter_creator: gitlab
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is Social Advocacy? 

Through a [social media advocacy program](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/team-member-social-advocacy/), we are able to provide GitLab team members fresh news and updates about our organization and as well as throughout our industry. By having a tool in place that provides curated content, you'll be able to build your personal brand, become an expert in your topic, and contribute  to the growth of GitLab.

Curators are selected intentionally by the Social Media team to drive our advocacy content strategy. You are responsible for curating stories to be shared by GitLab team members by adding content to the Bambu platform. Each piece of content added to Bambu should benefit your team or your topic. Please take the training and complete the issue you are assigned to. This will certify you as a curator in this program. 

##### 🔗 [Watch the training here. You'll need to be logged into GitLab's Unfiltered YouTube account in order to access the video.](https://youtu.be/HqeGYKkcovs)


</details>

<details>
 <summary markdown='span'>
 What can I expect out of the training?
 </summary>

This training is conducted by a Strategic Services Consultant and provides a demonstration to help you navigate the platform, and understand the role of a curator. In this training, you will see the platform from a "reader" perspective, and curator's. This will help you learn the Bambu lifecycle. You will then learn how to set up a profile, connect your social media accounts, upload content to the platform, and view metrics. 

</details>

<details>
 <summary markdown='span'>
 This wasn't a live session, where can I ask remaining questions that I have?
 </summary>

Following the training, the social media team hosted an AMA so that curators could ask any remaining questions on the platform or the program. Questions and suggestions can still be submitted to the #social-advocacy-curators Slack channel. This channel will also help you stay in touch with the curator program and the latest news. This channel is for team members who are identified as content curators only.

##### 🔗 [Watch the AMA here. You'll need to be logged into GitLab's Unfiltered YouTube account in order to access the video.](https://youtu.be/tob7weMR-uc)

</details>

### Current list of curators 

| Name | Topic |
| ------ | ------ | 
| TBD | Awards and contributed articles |
| Jessica Reeder | All Remote |
| Nuritzi Sanchez | Open Source |
| Heather Simpson | Security | 
| Kira Aubrey | Field Marketing / PubSec |
| Madison Taft | Sales | 
| TBD | Blogs (may cross into other topics as needed) | 
| Suri Patel | GitOps, Brand & Values | 
| Fiona O'Keefe | Customer Reference | 
| Jennifer Parker | Customer Reference |
| Simon Lang | Internal Communications, Internal Stories |
| TBD | Talent Brand, Job postings |
| Wil Spillane | Admin/Manager, Brand & Values, News/Press |   

### It's a curators responsibility to make sure that the content they want team members to share is SAFE

When drafting social posts for team members to share, curators should be sure to answer these questions: 
- Does this material include any **Sensitive** or material nonpublic information?
- Is the information **Accurate** and verifiable? 
- Does this information include **Financial** data, metrics, or predictions?
- Could sharing this information have a harmful **Effect** on our team members or the business? 

[Please be sure to review the SAFE Handbook page for detailed information.](https://about.gitlab.com/handbook/legal/safe-framework/)

### Curator Checklist cheat sheet
When you add content into Bambu, consider the following: 

- Ensure content is SAFE
- Organize with topic and tag 
- Include contributor note as context for why the content is worth sharing and a short CTA: e.g. "This story helps to establish GitLabs all remote philosophy, share it with your communities."
- Include suggested social post copy
   - 1-2 versions for Twitter
   - 1-2 versions that cover both Facebook and LinkedIn
   - Diversify options: one copy for “professional” use (full words, no emojis, sounds clean and clear) and one copy for “fun” (emojis, exclamation points, light and fun).
- Add start and end date
   - Stories should be available for a minimum of 1 month from publishing (e.g, Sept 28th to Oct 28) however, consider your campaigns and communications focuses - big deal content could go on longer.

### Recordings for Curators
Private videos, must be logged into the GitLab Unfiltered YouTube channel to view

- [Curator Kick off Sept 2021](https://www.youtube.com/watch?v=BxX8fac1DqM)

### Formatting tips for curators 

- Always find and use Twitter handles in the copy suggestions for Twitter. 
- Never use any handles for copy suggestions for LinkedIn or Facebook, as they will not actually tag pages
- Never start a tweet with `@`. If you want to mention a handle first, add a `.`, so it would appear as `.@GitLab`. Without the period, Twitter will believe this is a response and not a broadcast, so the tweet will not appear on people's walls, dramatically reducing reach and making the post ineffective.
- When changing an image or story title, these changes will only appear on LinkedIn. The original title of the story and original image will appear on Facebook and Twitter.
- Regarding hashtags, when in doubt, leave it out. Platforms now focus on trends and topics without them. They can be helpful on LinkedIn, where users can follow a hashtag, but they are not required.
- When using hashtags, replace a word in the copy with a hashtag. Sometimes the tense and other elements don't allow this. Only then should you consider adding hashtags to the end of the copy suggestion. 
   - E.g. #MovingToGitLab: This works when we're using present tense in the copy, `Company x is #MovingToGitLab`. This won't work if we're referencing a move from the past. Where we'd say, `Company x migrated to GitLab and here's what they learned. #MovingToGitLab`
- Always add a CTA to the internal `note`: Consider sharing on social, please share on your channels, etc.
- Photo stories are not a priority and are only shareable on Facebook and Twitter, if we'd ever use these.
